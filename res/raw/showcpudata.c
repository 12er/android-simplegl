(vertex)

attribute vec3 position;
attribute vec2 uv;
varying vec2 vuv;

void main() {
	gl_Position = vec4(position + vec3(0,-1,0),1);
	vuv = uv;
}

(fragment)

precision mediump float;

varying vec2 vuv;

uniform sampler2D tex;

void main() {
	gl_FragColor = texture2D(tex,vuv);
}
