(vertex)

attribute vec3 position;
attribute vec2 uv;
varying vec2 v_uv;

void main() {
	gl_Position = vec4(position,1);
	v_uv = uv;
}

(fragment)

precision mediump float;
varying vec2 v_uv;

uniform sampler2D tex2;
uniform sampler2D tex1;


void main() {
	gl_FragColor = texture2D(tex1,v_uv)*(1.0-v_uv.x) + texture2D(tex2,v_uv)*v_uv.x;
}
