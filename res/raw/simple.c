(vertex)

attribute vec4 color;
attribute vec3 position;
varying vec4 vColor;
uniform vec3 translate;
void main() {
	gl_Position = vec4(position,1) + vec4(translate,0);
	vColor = color;
}

(fragment)

precision mediump float;
varying vec4 vColor;
void main(){
	gl_FragColor = vColor;
}