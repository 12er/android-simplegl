(vertex)

attribute vec3 position;
attribute vec4 color;
varying vec4 vColor;
void main() {
	vColor = vec4(1) - color;
	vec3 pos = position * 0.25 + vec3(-0.75,0.75,0);
	gl_Position = vec4(pos,1);
}
(fragment)

precision mediump float;
varying vec4 vColor;
void main() {
	gl_FragColor = vColor;
}
