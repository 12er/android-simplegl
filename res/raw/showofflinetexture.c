(vertex)

attribute vec3 position;
attribute vec2 uv;

varying vec2 v_uv;

void main() {
	gl_Position = vec4(position - vec3(1,1,0),1);
	v_uv = uv;
}

(fragment)

precision mediump float;

varying vec2 v_uv;

uniform sampler2D display;

void main() {
	//gl_FragColor = texture2D(display,v_uv);

	vec4 avgColor = vec4(0.0);

	for(float i=-2.0 ; i<=2.0 ; i++) {
		for(float j=-2.0 ; j<=2.0 ; j++) {
			avgColor += texture2D(display,v_uv + vec2(i,j)*0.01)*0.04;
		}
	}

	gl_FragColor = avgColor;
}
