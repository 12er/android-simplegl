package simplegl;

import java.util.HashMap;

public abstract class Uniform {
	
	private static int nextID = 0;
	
	private static int createNewID() {
		return nextID++;
	}
	
	protected int id;
	protected String mainVarName;
	private HashMap<Shader, Integer> handles;
	
	public Uniform(String mainVarName) {
		this.id = createNewID();
		this.mainVarName = mainVarName;
		handles = new HashMap<Shader,Integer>();
	}
	
	public int getID() {
		return id;
	}
	
	public void use(Shader shader) {
		use(shader,mainVarName);
	}
	
	public void use(Shader shader, String varname) {
		if(shader.isCompiled()) {
			int varHandle = -1;
			if(!handles.containsKey(shader)) {
				varHandle = shader.getUniformHandle(varname);
				if(varHandle != -1) {
					handles.put(shader, varHandle);
				}
			} else {
				varHandle = handles.get(shader);
			}
			if(varHandle != -1) {
				loadDataToShader(shader, varHandle);
			}
		}
	}
	
	protected abstract void loadDataToShader(Shader shader, int varHandle);

}
