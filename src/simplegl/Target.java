package simplegl;

import java.io.InputStream;
import java.util.HashMap;
import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.opengl.GLES20;
import android.opengl.GLUtils;

public class Target {
	
	//data before compilation
	private int nextWidth = -1;
	private int nextHeight = -1;
	private boolean nextRenderToDisplay = false;
	
	//compiled data
	private int targetHandle = 0;
	private int renderHandle = 0;
	private int width = -1;
	private int height = -1;
	private boolean renderToDisplay = false;
	
	public Target() {
	}
	
	public void setWidth(int width) {
		nextWidth = width;
	}
	
	public void setHeight(int height) {
		nextHeight = height;
	}
	
	public void setRenderToDisplay(boolean flag) {
		this.nextRenderToDisplay = flag;
	}
	
	public int getWidth() {
		return width;
	}
	
	public int getHeight() {
		return height;
	}
	
	public boolean isRenderingToDisplay() {
		return renderToDisplay;
	}
	
	public int getHandle() {
		return targetHandle;
	}
	
	public boolean isCompiled() {
		return width != -1 && height != -1;
	}
	
	public void compile() {
		if(width != -1 && height != -1) {
			width = -1;
			height = -1;
			if(!renderToDisplay) {
				int [] oldRenderHandle = new int[1];
				oldRenderHandle[0] = renderHandle;
				GLES20.glDeleteRenderbuffers(1, oldRenderHandle, 0);
				Err.c("glDeleteRenderbuffers");
				int [] oldHandle = new int[1];
				oldHandle[0] = targetHandle;
				GLES20.glDeleteFramebuffers(1, oldHandle, 0);
				Err.c("glDeleteFramebuffers");
				renderHandle = 0;
				targetHandle = 0;
			}
			renderToDisplay = false;
		}
		if(nextWidth < 1 || nextHeight < 1) {
			Err.e("Error: couldn't initialize a rendertarget, because no positive width or height was specified");
			return;
		}
		if(!nextRenderToDisplay) {
			int [] newTargetHandle = new int[1];
			GLES20.glGenFramebuffers(1, newTargetHandle, 0);
			Err.c("glGenFramebuffers");
			GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, newTargetHandle[0]);
			Err.c("glBindFramebuffer");
			int [] newRenderHandle = new int[1];
			GLES20.glGenRenderbuffers(1, newRenderHandle, 0);
			Err.c("glGenRenderbuffers");
			GLES20.glBindRenderbuffer(GLES20.GL_RENDERBUFFER, newRenderHandle[0]);
			Err.c("glBindRenderbuffer");
			GLES20.glRenderbufferStorage(GLES20.GL_RENDERBUFFER, GLES20.GL_DEPTH_COMPONENT16, nextWidth, nextHeight);
			Err.c("glRenderbufferStorage");
			GLES20.glFramebufferRenderbuffer(GLES20.GL_FRAMEBUFFER, GLES20.GL_DEPTH_ATTACHMENT, GLES20.GL_RENDERBUFFER, newRenderHandle[0]);
			Err.c("glFramebufferRenderbuffer");
			GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, 0);
			Err.c("glBindFramebuffer");
			targetHandle = newTargetHandle[0];
			renderHandle = newRenderHandle[0];
		}
		//finalize compilation by assigning values
		//to the properties width, height and textureHandle
		this.width = nextWidth;
		this.height = nextHeight;
		this.renderToDisplay = nextRenderToDisplay;
		
		//compilation successfull, reset next* properties
		nextWidth = nextHeight = -1;
		nextRenderToDisplay = false;
	}
	
	public void clear() {
		nextWidth = width = -1;
		nextHeight = height = -1;
		nextRenderToDisplay = false;
		if(!renderToDisplay) {
			int [] oldRenderHandle = new int[1];
			oldRenderHandle[0] = renderHandle;
			GLES20.glDeleteRenderbuffers(1, oldRenderHandle, 0);
			Err.c("glDeleteRenderbuffers");
			int [] oldHandle = new int[1];
			oldHandle[0] = targetHandle;
			GLES20.glDeleteFramebuffers(1, oldHandle, 0);
			Err.c("glDeleteFramebuffers");
			renderHandle = 0;
			targetHandle = 0;
		}
		renderToDisplay = false;
	}
	
	public void setViewport() {
		if(width == -1 || height == -1) {
			return;
		}
		GLES20.glViewport(0, 0, width, height);
	}
	
	public static void setViewport(int x, int y, int width, int height) {
		GLES20.glViewport(x, y, width, height);
	}
	
	public static void clearTarget() {
		GLES20.glClearColor(0, 0, 0, 1);
		GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);
	}
	
	public static void clearColorBuffer() {
		GLES20.glClearColor(0, 0, 0, 1);
		GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT);
	}
	
	public static void clearDepthBuffer() {
		GLES20.glClear(GLES20.GL_DEPTH_BUFFER_BIT);
	}
	
	public void beginCapture(Shader shader, List<Texture> textures) {
		if(renderToDisplay) {
			return;
		}
		//no support for multiple render targets in GLES20
		//GLES20.glPushAttrib(GLES20.GL_VIEWPORT_BIT | GLES20.GL_COLOR_BUFFER_BIT);
		if(width == -1 || height == -1 || !shader.isCompiled()) {
			return;
		}
		GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, targetHandle);
		for(Texture texture : textures) {
			//multitexturing does not work with GLES20
			//hence only lists with one texture in it make sense for now
			//in the future the locations will be added to GL_COLOR_ATTACHMENT0
			//and collected for use by glDrawBuffers
			//furthermore glGetFragDataLocation will need to be invoced to
			//get the slot associated with the texture's uniform name in the shader
			GLES20.glFramebufferTexture2D(GLES20.GL_FRAMEBUFFER, GLES20.GL_COLOR_ATTACHMENT0, GLES20.GL_TEXTURE_2D, texture.getHandle(), 0);
		}
		
		//no support for multiple render targets in GLES20
		//glDrawBuffers: rendertargets have to be added in the order
		//of the color attachment numbers, hence sort(array of drawBuffers)
		//will be necessary
		//GLES20.glDrawBuffers(num of texlocations, array of drawBuffers);
	}
	
	public void endCapture() {
		if(renderToDisplay) {
			return;
		}
		//no support for multiple render targets in GLES20
		//GLES20.glDrawBuffers(GLES20.GL_COLOR_ATTACHMENT0);
		//GLES20.glPopAttrib();
		GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER,0);
	}

}
