package simplegl;

import android.opengl.GLES20;

public class UFloat extends Uniform {
	
	public float value;
	
	public UFloat(String mainVarName) {
		super(mainVarName);
	}

	@Override
	protected void loadDataToShader(Shader shader, int varHandle) {
		GLES20.glUniform1f(varHandle, value);
	}

}
