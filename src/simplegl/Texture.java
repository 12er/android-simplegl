package simplegl;

import java.io.InputStream;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.util.HashMap;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.opengl.GLES20;
import android.opengl.GLUtils;

public class Texture extends Uniform {
	
	private Context context;
	private int mainSlot;
	private HashMap<Shader, Integer> slots;
	
	//data before compilation
	private int nextImage = 0;
	private Bitmap nextBitmap = null;
	private int nextWidth = -1;
	private int nextHeight = -1;
	
	//compiled data
	private int textureHandle = 0;
	private int width = -1;
	private int height = -1;
	
	public Texture(Context context, String varname, int mainSlot) {
		super(varname);
		this.context = context;
		this.mainSlot = mainSlot;
		slots = new HashMap<Shader, Integer>();
	}
	
	public void setImage(int resource) {
		nextImage = resource;
		nextBitmap = null;
	}
	
	public void setImage(Bitmap bitmap) {
		nextBitmap = bitmap;
		nextImage = 0;
	}
	
	public void setWidth(int width) {
		nextWidth = width;
	}
	
	public void setHeight(int height) {
		nextHeight = height;
	}
	
	public int getWidth() {
		return width;
	}
	
	public int getHeight() {
		return height;
	}
	
	public int getHandle() {
		return textureHandle;
	}
	
	public boolean isCompiled() {
		return width != -1 && height != -1;
	}
	
	public void compile() {
		if(width != -1 && height != -1) {
			width = -1;
			height = -1;
			int [] oldHandle = new int[1];
			oldHandle[0] = textureHandle;
			GLES20.glDeleteTextures(1, oldHandle, 0);
			Err.c("glDeleteTextures");
			textureHandle = 0;
		}
		if((nextWidth < 1 || nextHeight < 1) && nextImage == 0 && nextBitmap == null) {
			Err.e("Error: couldn't initialize a texture, because no positive width and height, or no image resource was specified");
			return;
		}
		if(nextImage != 0) {
			//try loading data from an image into the texture
			InputStream is = null;
			Bitmap originalBitmap = null;
			Bitmap bitmap = null;
			try {
				is = context.getResources().openRawResource(nextImage);
				originalBitmap = BitmapFactory.decodeStream(is);
			} catch(Exception e) {
				Err.e("Error: could not load image data from resource");
				return;
			} finally {
				try {
					is.close();
				} catch(Exception e) {
					//Ignore
				}
			}
			//in opengl (0,0) is the left bottom image corner
			//but android bitmaps use (0,0) as the left top image corner
			//hence flipping the image is necessary
			try {
				Matrix flipMatrix = new Matrix();
				flipMatrix.preScale(1, -1);
				bitmap = Bitmap.createBitmap(originalBitmap, 0, 0, originalBitmap.getWidth(), originalBitmap.getHeight(), flipMatrix, true);
			} catch(Exception e) {
				Err.e(e.getStackTrace());
				return;
			} finally {
				originalBitmap.recycle();
			}
			//loading image is done
			//now initialize the texture, and upload the image data
			int []freshHandle = new int[1];
			GLES20.glGenTextures(1, freshHandle, 0);
			Err.c("glGenTextures");
			if(freshHandle[0] == 0) {
				Err.e("Error: could not initialize texture");
				bitmap.recycle();
				return;
			}
			GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, freshHandle[0]);
			Err.c("glBindTexture");
			GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_LINEAR);
			Err.c("glTexParameterf - GL_TEXTURE_MIN_FILTER");
			GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR);
			Err.c("glTexParameterf - GL_TEXTURE_MAG_FILTER");
			GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_S, GLES20.GL_REPEAT);
			Err.c("glTexParameteri - GL_TEXTURE_WRAP_S");
			GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_T, GLES20.GL_REPEAT);
			Err.c("glTexParameteri - GL_TEXTURE_WRAP_T");
			GLUtils.texImage2D(GLES20.GL_TEXTURE_2D, 0, bitmap, 0);
			Err.c("glTexImage2D");
			//finalize compilation by assigning values
			//to the properties width, height and textureHandle
			this.width = bitmap.getWidth();
			this.height = bitmap.getHeight();
			this.textureHandle = freshHandle[0];
			bitmap.recycle();
		} else if(nextBitmap != null) {
			//in opengl (0,0) is the left bottom image corner
			//but android bitmaps use (0,0) as the left top image corner
			//hence flipping the image is necessary
			Bitmap bitmap = null;
			try {
				Matrix flipMatrix = new Matrix();
				flipMatrix.preScale(1, -1);
				bitmap = Bitmap.createBitmap(nextBitmap, 0, 0, nextBitmap.getWidth(), nextBitmap.getHeight(), flipMatrix, true);
			} catch(Exception e) {
				Err.e(e.getStackTrace());
				return;
			}
			//loading image is done
			//now initialize the texture, and upload the image data
			int []freshHandle = new int[1];
			GLES20.glGenTextures(1, freshHandle, 0);
			Err.c("glGenTextures");
			if(freshHandle[0] == 0) {
				Err.e("Error: could not initialize texture");
				bitmap.recycle();
				return;
			}
			GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, freshHandle[0]);
			Err.c("glBindTexture");
			GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_LINEAR);
			Err.c("glTexParameterf - GL_TEXTURE_MIN_FILTER");
			GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR);
			Err.c("glTexParameterf - GL_TEXTURE_MAG_FILTER");
			GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_S, GLES20.GL_REPEAT);
			Err.c("glTexParameteri - GL_TEXTURE_WRAP_S");
			GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_T, GLES20.GL_REPEAT);
			Err.c("glTexParameteri - GL_TEXTURE_WRAP_T");
			GLUtils.texImage2D(GLES20.GL_TEXTURE_2D, 0, bitmap, 0);
			Err.c("glTexImage2D");
			//finalize compilation by assigning values
			//to the properties width, height and textureHandle
			this.width = bitmap.getWidth();
			this.height = bitmap.getHeight();
			this.textureHandle = freshHandle[0];
			bitmap.recycle();
		} else {
			//no image resource given
			//hence initialize a texture with width and height
			//specified by the setWidth/setHeight methods
			//now initialize the texture, and upload the image data
			Bitmap bitmap = null;
			try {
				bitmap = Bitmap.createBitmap(nextWidth, nextHeight, Bitmap.Config.ARGB_8888);
			} catch(Exception e) {
				Err.e("Error: could not load image data from resource");
				return;
			}
			int []freshHandle = new int[1];
			GLES20.glGenTextures(1, freshHandle, 0);
			Err.c("glGenTextures");
			if(freshHandle[0] == 0) {
				Err.e("Error: could not initialize texture");
				return;
			}
			GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, freshHandle[0]);
			Err.c("glBindTexture");
			GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_LINEAR);
			Err.c("glTexParameterf - GL_TEXTURE_MIN_FILTER");
			GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR);
			Err.c("glTexParameterf - GL_TEXTURE_MAG_FILTER");
			GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_S, GLES20.GL_REPEAT);
			Err.c("glTexParameteri - GL_TEXTURE_WRAP_S");
			GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_T, GLES20.GL_REPEAT);
			Err.c("glTexParameteri - GL_TEXTURE_WRAP_T");
			GLUtils.texImage2D(GLES20.GL_TEXTURE_2D, 0, bitmap, 0);
			Err.c("glTexImage2D");
			//finalize compilation by assigning values
			//to the properties width, height and textureHandle
			this.width = nextWidth;
			this.height = nextHeight;
			this.textureHandle = freshHandle[0];
			bitmap.recycle();
		}
		
		//compilation successfull, reset next* properties
		nextWidth = nextHeight = -1;
		nextImage = 0;
		nextBitmap = null;
	}
	
	public void clear() {
		nextWidth = width = -1;
		nextHeight = height = -1;
		nextImage = 0;
		nextBitmap = null;
		int [] oldHandle = new int[1];
		oldHandle[0] = textureHandle;
		GLES20.glDeleteTextures(1, oldHandle, 0);
		Err.c("glDeleteTextures");
		textureHandle = 0;
	}
	
	public void changeImage(Buffer buffer) {
		if(width == -1 || height == -1) {
			return;
		}
		GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, textureHandle);
		GLES20.glTexSubImage2D(GLES20.GL_TEXTURE_2D, 0, 0, 0, width, height, GLES20.GL_RGBA, GLES20.GL_UNSIGNED_BYTE, buffer);
		Err.c("glTexSubImage2D");
	}
	
	public void setSlot(Shader shader, int slot) {
		slots.put(shader, slot);
	}
	
	public void useMainSlot(Shader shader) {
		if(slots.containsKey(shader)) {
			slots.remove(shader);
		}
	}
	
	public void useMainSlot() {
		slots.clear();
	}

	@Override
	protected void loadDataToShader(Shader shader, int varHandle) {
		int slot = mainSlot;
		if(slots.containsKey(shader)) {
			slot = slots.get(shader);
		}
		GLES20.glActiveTexture(GLES20.GL_TEXTURE0 + slot);
		GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, textureHandle);
		GLES20.glUniform1i(varHandle, slot);
	}

}
