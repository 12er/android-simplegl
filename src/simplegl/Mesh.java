package simplegl;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.content.Context;
import android.opengl.GLES20;
import android.util.Log;

public class Mesh {
	
	private Context context;
	private int verticesPerPolygon = 3;
	
	//data before compilation
	private HashMap<String, VBuffer> nextBuffers;
	private int nextVertexCount = 0;
	
	//compiled data
	private FloatBuffer buffer = null;
	private List<String> attributeNames;
	private List<Integer> attributeOffsets;
	private List<Integer> attributeDimensions;
	private HashMap<Shader,List<Integer>> attributeLocations;
	private int vertexCount = 0;
	private int stride = 0;
	
	public Mesh(Context context) {
		this.context = context;
		nextBuffers = new HashMap<String, VBuffer>();
		attributeNames = new ArrayList<String>();
		attributeOffsets = new ArrayList<Integer>();
		attributeDimensions = new ArrayList<Integer>();
		attributeLocations = new HashMap<Shader,List<Integer>>();
	}
	
	public void addBuffers(int fileresource) {
		//read text in file
		List<String> code = null;
		try {
			code = readFile(fileresource);
		} catch(Exception e) {
			Err.e(e.getStackTrace());
			return;
		}
		
		//parse file
		int lineno = 0;
		int vertPerPoly = 0;
		String currentBufferName = new String();
		List<VBuffer> buffers = new LinkedList<VBuffer>();
		int currentDimension = 0;
		List<Float> currentBuffer = null;
		for(String line : code) {
			lineno++;
			
			//collect number list, if possible
			List<Float> numberlist = new LinkedList<Float>();
			String stringlist[] = line.split("(\t|\r|\\s)+");
			String lineWithoutSpace = new String();
			boolean isnumberlist = true;
			for(String entry : stringlist) {
				if(entry.length() == 0) {
					continue;
				}
				lineWithoutSpace += entry;
				try {
					numberlist.add(Float.valueOf(entry));
				} catch(Exception e) {
					isnumberlist = false;
				}
			}
			//some cases can occur: the current line is a nonempty numberlist
			//or it is an assignment of verticesPerPolygon,
			//or it is a buffername such that the numbers below this line belong to the new buffer
			//a list of numbers without a buffername before cannot be accepted
			int assignmentIndex = -1;
			if(isnumberlist && lineWithoutSpace.length() != 0) {
				if(currentBuffer == null) {
					Err.e("Error: error at line " + lineno + " in mesh file, there must be an buffer list before any listing of numbers");
					return;
				}
				if(currentDimension == 0) {
					currentDimension = numberlist.size();
				} else if(currentDimension != numberlist.size()) {
					Err.e("Error: error at line " + lineno + " in mesh file, each line of a buffer must have the same number of components");
					return;
				}
				currentBuffer.addAll(numberlist);
			} else if(lineWithoutSpace.length() != 0) {
				if(lineWithoutSpace.charAt(0) == '(' && lineWithoutSpace.charAt(lineWithoutSpace.length()-1) == ')') {
					String bufferName = lineWithoutSpace.substring(1,lineWithoutSpace.length()-1);
					if(!bufferName.matches("[a-zA-Z_][a-zA-Z_0-9]*")) {
						Err.e("Error: syntax error in assignment in mesh file at lineno " + lineno + ", buffername must be a regexp matching [a-zA-Z_][a-zA-Z_0-9]*");
						return;
					}
					if(currentBuffer != null) {
						if(currentBuffer.size() == 0) {
							Err.e("Error: located an attribute in mesh file without any attributes");
							return;
						}
						VBuffer newVBuffer = new VBuffer();
						newVBuffer.name = currentBufferName;
						newVBuffer.buffer = new float[currentBuffer.size()];
						for(int ind = 0 ; ind<currentBuffer.size() ; ind++) {
							newVBuffer.buffer[ind] = currentBuffer.get(ind);
						}
						newVBuffer.dimension = currentDimension;
						buffers.add(newVBuffer);
					}
					currentBuffer = new LinkedList<Float>();
					currentDimension = 0; //dimension 0 -> next line determines dimension
					currentBufferName = bufferName;
				} else if((assignmentIndex = lineWithoutSpace.indexOf("=")) != -1) {
					try {
						String varname = lineWithoutSpace.substring(0,assignmentIndex);
						String valueStr = lineWithoutSpace.substring(assignmentIndex+1);
						if(!varname.equals("verticesPerPolygon")) {
							throw new Exception();
						}
						vertPerPoly = Integer.valueOf(valueStr);
					} catch(Exception e) {
						Err.e("Error: syntax error in assignment in mesh file at lineno " + lineno);
						return;
					}
				} else {
					Err.e("Error: syntax error in mesh file at lineno " + lineno);
					return;
				}
			}
		}
		if(vertPerPoly <= 0) {
			Err.e("Error: verticesPerPolygon in mesh file not specified or negative");
			return;
		}
		//at least one buffer exists
		//add the last buffer to the buffers
		if(currentBuffer.size() == 0) {
			Err.e("Error: located an attribute in mesh file without any attributes");
			return;
		}
		VBuffer newVBuffer = new VBuffer();
		newVBuffer.name = currentBufferName;
		newVBuffer.buffer = new float[currentBuffer.size()];
		for(int ind = 0 ; ind<currentBuffer.size() ; ind++) {
			newVBuffer.buffer[ind] = currentBuffer.get(ind);
		}
		newVBuffer.dimension = currentDimension;
		buffers.add(newVBuffer);
		
		
		//append buffers to mesh
		setVerticesPerPolygon(vertPerPoly);
		for(VBuffer vb : buffers) {
			addBuffer(vb.name, vb.buffer, vb.dimension);
		}
	}
	
	private List<String> readFile(int fileresource) throws Exception {
		InputStream inputStream = context.getResources().openRawResource(fileresource);
		BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
		List<String> stringList = new LinkedList<String>();
		String line;
		while((line = reader.readLine()) != null) {
			stringList.add(line);
		}
		
		return stringList;
	}
	
	public void addBuffer(String name, float buffer[], int dimension) {
		if(dimension < 1 || dimension > 4) {
			Err.e("Error: a vertex attribute must have 1,2,3 or 4 components per vertex");
			return;
		}
		
		//calculate the number of polygons in the next buffers, and in the
		//buffer about to be added
		int currentPolygonCount = nextVertexCount / verticesPerPolygon;
		float temp = ((float)buffer.length)/((float)dimension)/((verticesPerPolygon));
		int bufferPolygonCount = (int)Math.ceil(temp);
		int newPolygonCount = Math.max(currentPolygonCount, bufferPolygonCount);
		
		//update the number of vertices
		nextVertexCount = newPolygonCount * verticesPerPolygon;
		
		//append zeros to the buffers such that all buffers contain
		//the same number of polygons
		int newVertices = verticesPerPolygon*(newPolygonCount-currentPolygonCount);
		if(newVertices > 0) {
			for(VBuffer b : nextBuffers.values()) {
				int newZeros = newVertices * b.dimension;
				float [] oldArray = b.buffer;
				b.buffer = new float[oldArray.length + newZeros];
				for(int i=0 ; i<oldArray.length ; i++) {
					b.buffer[i] = oldArray[i];
				}
				for(int i=oldArray.length ; i<oldArray.length+newZeros ; i++) {
					b.buffer[i] = 0;
				}
			}
		}
		
		//append zeros to the new buffer if necessary
		int newArraySize = nextVertexCount * dimension;
		if(buffer.length < newArraySize) {
			float [] oldArray = buffer;
			buffer = new float[newArraySize];
			for(int i=0 ; i<oldArray.length ; i++) {
				buffer[i] = oldArray[i];
			}
			for(int i=oldArray.length ; i<newArraySize ; i++) {
				buffer[i] = 0;
			}
		}
		
		//finally add buffer
		if(nextBuffers.containsKey(name)) {
			nextBuffers.remove(name);
		}
		VBuffer vBuffer = new VBuffer();
		vBuffer.name = name;
		vBuffer.buffer = buffer;
		vBuffer.dimension = dimension;
		nextBuffers.put(name, vBuffer);
	}
	
	public void compile() {
		//remove buffer previously compiled buffer
		buffer = null;
		vertexCount = 0;//
		stride = 0;//
		attributeNames.clear();
		attributeOffsets.clear();
		attributeDimensions.clear();
		attributeLocations.clear();
		
		//fill lists of names, dimensions and offsets, calculate stride
		Vector<VBuffer> nBuffers = new Vector<VBuffer>(nextBuffers.values());
		stride = 0;
		int offset = 0;
		vertexCount = nextVertexCount;
		for(VBuffer b : nBuffers) {
			stride += b.dimension * 4; //4 bytes per component
			attributeNames.add(b.name);
			attributeDimensions.add(b.dimension);
			attributeOffsets.add(offset);
			offset += b.dimension;
		}
		
		//compile actual buffer by interleaving buffers
		float farray[] = new float[vertexCount * stride / 4];
		int row = stride / 4;
		for(int bi = 0 ; bi<nBuffers.size() ; bi++) {
			offset = attributeOffsets.get(bi);
			int dimension = attributeDimensions.get(bi);
			float [] currentArray = nBuffers.get(bi).buffer;
			for(int i=0 ; i<vertexCount ; i++) {
				for(int j=0 ; j<dimension ; j++) {
					farray[i*row + j + offset] = currentArray[i*dimension + j];
				}
			}
		}
		buffer = ByteBuffer.allocateDirect(vertexCount * stride).order(ByteOrder.nativeOrder()).asFloatBuffer();
		buffer.put(farray).position(0);
		
		//prepare for next precompilatory state
		nextBuffers.clear();
		nextVertexCount = 0;
	}
	
	public void clear() {
		buffer = null;
		nextBuffers.clear();
		nextVertexCount = 0;
		attributeNames.clear();
		attributeOffsets.clear();
		attributeDimensions.clear();
		attributeLocations.clear();
		vertexCount = 0;
		stride = 0;
	}
	
	public boolean isCompiled() {
		return buffer != null;
	}
	
	public void render(Shader shader) {
		//terminate if the shader or this is not compiled
		if(!shader.isCompiled()) {
			Err.e("Error: trying to render a mesh with a shader, which was not compiled"); 
			return;
		}
		if(buffer == null) {
			Err.e("Error: trying to render a mesh, which is not compiled");
			return;
		}
		
		//collect attribute locations in the shader
		List<Integer> sLocations = null;
		if(attributeLocations.containsKey(shader)) {
			sLocations = attributeLocations.get(shader);
		} else {	
			sLocations = new ArrayList<Integer>();
			
			for(String name : attributeNames) {
				int loc = GLES20.glGetAttribLocation(shader.getHandle(), name);
				Err.c("glGetAttribLocation " + name);
				if(loc == -1) {
					Err.e("Error: could not locate attribute " + name + " in a shader");
					return;
				}
				sLocations.add(loc);
			}
			
			attributeLocations.put(shader, sLocations);
		}
		
		//activate the vertex attribute locations
		//and point to those
		Iterator<Integer> locIter = sLocations.iterator();
		Iterator<Integer> dimensionIter = attributeDimensions.iterator();
		Iterator<Integer> offsetIter = attributeOffsets.iterator();
		while(locIter.hasNext()) {
			int location = locIter.next();
			int dimension = dimensionIter.next();
			int offset = offsetIter.next();
			buffer.position(offset);
			GLES20.glVertexAttribPointer(location, dimension, GLES20.GL_FLOAT, false, stride, buffer);
			Err.c("glVertexAttribPointer");
			GLES20.glEnableVertexAttribArray(location);
			Err.c("glEnableVertexAttribArray");
		}
		
		//do the rendering
		if(verticesPerPolygon == 3) {
			GLES20.glDrawArrays(GLES20.GL_TRIANGLES, 0, vertexCount);
		} else if(verticesPerPolygon == 2) {
			GLES20.glDrawArrays(GLES20.GL_LINES, 0, vertexCount);
		} else if(verticesPerPolygon == 1) {
			GLES20.glDrawArrays(GLES20.GL_POINTS, 0, vertexCount);
		}
		
		//disable vertex attribute locations
		locIter = sLocations.iterator();
		while(locIter.hasNext()) {
			GLES20.glDisableVertexAttribArray(locIter.next());
			Err.c("glDisableVertexAttribArray");
		}
	}
	
	public void setVerticesPerPolygon(int number) {
		if(number < 1 || number > 3) {
			Err.e("Error: can change number of vertices per polygon only to 1,2 or 3");
			return;
		}
		if(number != verticesPerPolygon) {
			//delete compiled and uncompiled buffers
			//as their content won't make any sence
			//after the change of verticesPerPolygon
			clear();
		}
		verticesPerPolygon = number;
	}
	
	public int getVerticesPerPolygon() {
		return verticesPerPolygon;
	}
	
	private class VBuffer {
		public String name;
		public float [] buffer;
		int dimension;
	}

}
