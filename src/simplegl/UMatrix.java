package simplegl;

import android.opengl.GLES20;

public class UMatrix extends Uniform{
	
	public float components[];
	private int dimension;
	
	public UMatrix(String mainVarName, int dimension) {
		super(mainVarName);
		this.dimension = dimension;
		components = new float[dimension*dimension];
		if (dimension < 2 || dimension > 4) {
			Err.e("Error: Uniform Matrix " + mainVarName + " must be of type R^2x2, R^3x3 or R^4x4");
		}
	}

	@Override
	protected void loadDataToShader(Shader shader, int varHandle) {
		if(dimension == 4) {
			GLES20.glUniformMatrix4fv(varHandle, 1, false, components, 0);
		} else if(dimension == 3) {
			GLES20.glUniformMatrix3fv(varHandle, 1, false, components, 0);
		} else if(dimension == 2) {
			GLES20.glUniformMatrix2fv(varHandle, 1, false, components, 0);
		}
	}
	
	public int getDimension() {
		return dimension;
	}

}
