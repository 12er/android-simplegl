package simplegl;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import android.content.Context;
import android.opengl.GLES20;
import android.util.Log;

public class Shader {
	
	private Context context;
	private List<String> code = new ArrayList<String>();
	private List<Integer> codeType = new ArrayList<Integer>();
	private boolean compiled = false;
	private String log;
	private int programHandle = 0;
	
	public Shader(Context context) {
		this.context = context;
	}
	
	public void addShader(int fileresource) {
		String code;
		try {
			code = readFile(fileresource);
		} catch(Exception e) {
			Err.e(e.getStackTrace());
			return;
		}
		addShaderCode(code);
	}
	
	public void addShader(int type, int fileresource) {
		String code;
		try {
			code = readFile(fileresource);
		} catch(Exception e) {
			Err.e(e.getStackTrace());
			return;
		}
		addShaderCode(type, code);
	}
	
	private String readFile(int fileresource) throws Exception {
		InputStream inputStream = context.getResources().openRawResource(fileresource);
		BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
		StringBuilder text = new StringBuilder();
		String line;
		while((line = reader.readLine()) != null) {
			text.append(line + "\n");
		}
		
		return text.toString();
	}
	
	public void addShaderCode(String code) {
		int currentType = GLES20.GL_VERTEX_SHADER;
		int vIndex = code.indexOf("(vertex)");
		int fIndex = code.indexOf("(fragment)");
		int index = -1;
		if(vIndex != 0 && fIndex != 0) {
			String errmsg = "Error: there must be at least one tag (vertex) or (fragment) in the first line starting with the first character of the code. Those tags can be used multiple times in the code. The section after the tag (vertex) is used as a vertex shader, and the section after the tag (fragment) is used as a fragment shader";
			log += errmsg;
			Err.e(errmsg);
			return;
		}
		
		//determine shader type of first section
		if(vIndex == 0) {
			currentType = GLES20.GL_VERTEX_SHADER;
			code = code.substring(8);
		} else if(fIndex == 0) {
			currentType = GLES20.GL_FRAGMENT_SHADER;
			code = code.substring(10);
		}
		
		//iteratate through (vertex) and (fragment) tags
		//and split the fragments of code
		do {
			vIndex = code.indexOf("(vertex)");
			fIndex = code.indexOf("(fragment)");
			int lastIndex = code.length();
			int tagLength = 0;
			int nextType = GLES20.GL_VERTEX_SHADER;
			
			//identify position and type of next section
			if(0 <= vIndex && (vIndex <= fIndex || fIndex == -1)) {
				//the next tag is a (vertex) tag
				//the current section is ending there
				lastIndex = vIndex;
				tagLength = 8;
				nextType = GLES20.GL_VERTEX_SHADER;
			} else if(0 <= fIndex) {
				//the next tag is a (fragment) tag
				//the current section is ending there
				lastIndex = fIndex;
				tagLength = 10;
				nextType = GLES20.GL_FRAGMENT_SHADER;
			}
			
			//use current section
			String currentSection = code.substring(0,lastIndex);
			addShaderCode(currentType, currentSection);
			
			//prepare code for next iteration
			code = code.substring(lastIndex + tagLength);
			currentType = nextType;
		} while( //finish, if no tags are present in code
				vIndex != -1 ||
				fIndex != -1
				);
	}
	
	public void addShaderCode(int type, String code) {
		this.code.add(code);
		this.codeType.add(type);
	}
	
	public void compile() {
		if(compiled) {
			GLES20.glDeleteProgram(programHandle);
			Err.c("glDeleteProgram");
			programHandle = 0;
			compiled = false;
		}
		Iterator<String> codeIter = code.iterator();
		Iterator<Integer> typeIter = codeType.iterator();
		List<Integer> compiledShaders = new ArrayList<Integer>();
		while(codeIter.hasNext() && typeIter.hasNext()) {
			String c = codeIter.next();
			int t = typeIter.next();
			int shader = compileShader(t, c);
			if(shader == 0) {
				log += "Error: not linking program due to compilation errors";
				Err.e(log);
				for(Integer cs : compiledShaders) {
					GLES20.glDeleteShader(cs);
					Err.c("glDeleteShader");
				}
				return;
			}
			compiledShaders.add(shader);
		}
		Log.i(Err.TAG, "Continue with linking");
		
		programHandle = GLES20.glCreateProgram();
		Err.c("glCreateProgram");
		if(programHandle != 0) {
			for(Integer cs : compiledShaders) {
				GLES20.glAttachShader(programHandle, cs);
				Err.c("glAttachShader");
			}
			GLES20.glLinkProgram(programHandle);
			int linkStatus[] = new int[1];
			GLES20.glGetProgramiv(programHandle, GLES20.GL_LINK_STATUS, linkStatus,0);
			if(linkStatus[0] != GLES20.GL_TRUE) {
				String errlg = GLES20.glGetProgramInfoLog(programHandle);
				log += errlg;
				Err.e("Could not link program, Errorlog: " + errlg);
				GLES20.glDeleteProgram(programHandle);
				programHandle = 0;
			} else {
				compiled = true;
				Log.i(Err.TAG, "Done with compiling shader");
				
				String warninglg = GLES20.glGetProgramInfoLog(programHandle);
				log += warninglg;
				if(warninglg != null && warninglg.length() > 0) {
					Err.w(warninglg);
				}
			}
			for(Integer cs : compiledShaders) {
				GLES20.glDeleteShader(cs);
				Err.c("glDeleteShader");
			}
		}
	}
	
	private int compileShader(int shaderType, String code) {
		int shader = GLES20.glCreateShader(shaderType);
		Err.c("glCreateShader");
		if(shader != 0) {
			GLES20.glShaderSource(shader, code);
			GLES20.glCompileShader(shader);
			int []compiled = new int[1];
			GLES20.glGetShaderiv(shader, GLES20.GL_COMPILE_STATUS, compiled,0);
			Err.c("glCompileShader");
			if(compiled[0] != GLES20.GL_TRUE) {
				String errlg = GLES20.glGetShaderInfoLog(shader);
				log += errlg;
				Err.e("Error: could not compile shader, Errorlog: " + errlg);
				GLES20.glDeleteShader(shader);
				shader = 0;
			} else {
				String warninglg = GLES20.glGetShaderInfoLog(shader);
				log += warninglg;
				if(warninglg != null && warninglg.length() > 0) {
					Err.w(warninglg);
				}
			}
		}
		return shader;
	}
	
	public void use() {
		if(compiled) {
			GLES20.glUseProgram(programHandle);
			Err.c("glUseProgram");
		}
	}
	
	public void clear() {
		GLES20.glDeleteProgram(programHandle);
		Err.c("glDeleteProgram");
		programHandle = 0;
		log = "";
		code.clear();
		codeType.clear();
		compiled = false;
	}
	
	public boolean isCompiled() {
		return compiled;
	}
	
	public int getHandle() {
		return programHandle;
	}
	
	public int getUniformHandle(String varname) {
		int varhandle = GLES20.glGetUniformLocation(programHandle, varname);
		Err.c_w("glGetUniformLocation " + varname);
		if(varhandle == -1) {
			Err.w("Warning: could not locate uniform variable " + varname);
		}
		return varhandle;
	}

}
