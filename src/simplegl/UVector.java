package simplegl;

import android.opengl.GLES20;

public class UVector extends Uniform {
	
	public float components[];
	
	public UVector(String mainVarName, int dimension) {
		super(mainVarName);
		components = new float[dimension];
		if (dimension < 1 || dimension > 4) {
			Err.e("Error: Uniform Vector " + mainVarName + " must be 1, 2, 3 or 4 components");
		}
	}

	@Override
	protected void loadDataToShader(Shader shader, int varHandle) {
		if(components.length == 3) {
			GLES20.glUniform3fv(varHandle, 1, components,0);
		} else if(components.length == 2) {
			GLES20.glUniform2fv(varHandle, 1, components,0);
		} else if(components.length == 4) {
			GLES20.glUniform4fv(varHandle, 1, components,0);
		} else if(components.length == 1) {
			GLES20.glUniform1fv(varHandle, 1, components,0);
		}
	}

}
