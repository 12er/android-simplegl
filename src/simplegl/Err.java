package simplegl;

import android.opengl.GLES20;
import android.util.Log;

public class Err {
	
	public static String TAG = "";
	private static boolean error = false;
	private static String errorString = "GLES20ExampleRenderer";
	
	public static void clear() {
		error = false;
		errorString = "";
	}
	
	public static void c(String op) {
		int errorType;
		while((errorType = GLES20.glGetError()) != GLES20.GL_NO_ERROR) {
			error = true;
			errorString = op + ": glError " + errorType;
			Log.e(TAG, "" + errorString);
		}		
	}
	
	public static void c_w(String op) {
		int errorType;
		while((errorType = GLES20.glGetError()) != GLES20.GL_NO_ERROR) {
			errorString = op + ": glWarning " + errorType;
			Log.w(TAG, "" + errorString);
		}
	}
	
	public static void e(String errorMessage) {
		error = true;
		errorString = errorMessage;
		Log.e(TAG, "" + errorMessage);
	}
	
	public static void e(StackTraceElement stackTrace[]) {
		error = true;
		errorString = "";
		if(stackTrace == null) {
			return;
		}
		for(StackTraceElement e : stackTrace) {
			errorString += e.toString();
		}
		Log.e(TAG, "" + errorString);
	}
	
	public static void w(String errorMessage) {
		errorString = errorMessage;
		Log.w(TAG, "" + errorMessage);
	}
	
	public static void w(StackTraceElement stackTrace[]) {
		errorString = "";
		for(StackTraceElement e : stackTrace) {
			errorString += e.toString();
		}
		Log.w(TAG, "" + errorString);
	}

	public static boolean hasError() {
		return error;
	}
	
	public static String getErrorString() {
		return errorString;
	}
	
}
