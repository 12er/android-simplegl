package com.example.simplegl;

import android.content.Context;
import android.opengl.GLSurfaceView;

public class GLView extends GLSurfaceView {
	
	public GLView(Context context) {
		super(context);
		setEGLContextClientVersion(2);
		setRenderer(new ExampleRenderer(context));
	}

}
