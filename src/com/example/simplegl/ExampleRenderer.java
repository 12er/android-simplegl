package com.example.simplegl;

import simplegl.*;

import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.CharBuffer;
import java.nio.FloatBuffer;
import java.util.Arrays;
import java.util.LinkedList;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import android.content.Context;
import android.opengl.*;
import android.os.SystemClock;
import android.util.Log;

public class ExampleRenderer implements GLSurfaceView.Renderer {
	
	private Context context;
	private long lastTime = Long.MAX_VALUE;
	private float deltaTime;
	private static String TAG = "GLES20ExampleRenderer";
	
	private Mesh mesh;
	private Mesh mesh2;
	private Mesh mesh3;
	
	private Texture texture;
	private Texture texture2;
	private Texture targetTexture;
	private Texture cpucopytex;
	
	private byte []cpubuffer;
	private ByteBuffer buffer;
	
	private Target screentarget;
	private Target target;
	
	private UVector translate;
	private UVector translate2;
	
	private Shader shader;
	private Shader shader2;
	private Shader shader3;
	private Shader shader4;
	private Shader shader5;
	
	public ExampleRenderer(Context context) {
		this.context = context;
	}

	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config) {
		//get opengl version
		Log.i(TAG,"" + GLES20.glGetString(GLES20.GL_VERSION));
		Log.i(TAG,"" + GLES20.glGetString(GLES20.GL_SHADING_LANGUAGE_VERSION));
		
		//check for errors
		Err.c("init");
		
		//use depth buffering
		GLES20.glEnable(GLES20.GL_DEPTH_TEST);
		
		//init programs
		shader = new Shader(context);
		shader.addShader(R.raw.simple);
		shader.compile();
		shader2 = new Shader(context);
		shader2.addShader(R.raw.othershader);
		shader2.compile();
		shader3 = new Shader(context);
		shader3.addShader(R.raw.texshader);
		shader3.compile();
		shader4 = new Shader(context);
		shader4.addShader(R.raw.showofflinetexture);
		shader4.compile();
		shader5 = new Shader(context);
		shader5.addShader(R.raw.showcpudata);
		shader5.compile();
		
		//init meshes
		mesh = new Mesh(context);
		mesh.addBuffers(R.raw.triangle);
		mesh.compile();
		mesh2 = new Mesh(context);
		mesh2.addBuffers(R.raw.example);
		mesh2.compile();
		mesh3 = new Mesh(context);
		mesh3.addBuffers(R.raw.texquad);
		mesh3.compile();
		
		//init textures
		texture = new Texture(context, "tex1", 0);
		texture.setImage(R.raw.palm);
		texture.compile();
		texture2 = new Texture(context, "tex2", 0);
		texture2.setImage(R.raw.sx);
		texture2.compile();
		targetTexture = new Texture(context,"display",0);
		targetTexture.setWidth(128);
		targetTexture.setHeight(128);
		targetTexture.compile();
		cpucopytex = new Texture(context, "tex", 0);
		cpucopytex.setWidth(64);
		cpucopytex.setHeight(128);
		cpucopytex.compile();
		//texture and texture2 slots collide if used by the same shader
		//hence map both textures to different slots for shader3
		texture.setSlot(shader3, 1);
		texture2.setSlot(shader3, 0);
		
		//cpu <-> gpu tex
		cpubuffer = new byte[cpucopytex.getWidth()*cpucopytex.getHeight()*4];
		buffer = ByteBuffer.allocateDirect(cpubuffer.length).order(ByteOrder.nativeOrder());
		buffer.put(cpubuffer).position(0);
		
		//initialize render targets
		screentarget = new Target();
		target = new Target();
		target.setWidth(targetTexture.getWidth());
		target.setHeight(targetTexture.getHeight());
		target.compile();
		
		//get translation variable location in shader
		translate = new UVector("translate", 3);
		translate2 = new UVector("translate", 3);
	}
	
	@Override
	public void onDrawFrame(GL10 arg0) {
		//update delta time
		deltaTime = Math.max((SystemClock.uptimeMillis()-lastTime)/1000.0f, 0);
		lastTime = SystemClock.uptimeMillis();
		
		//change translation
		float absval = 0;
		for(int i=0 ; i<3 ; i++) {
			absval += translate.components[i]*translate.components[i];
		}
		translate.components[1] += deltaTime;
		if(Math.sqrt(absval) > 2) {
			translate.components[0] = translate.components[1] = translate.components[2] = 0;
		}
		
		//change other translation
		absval = 0;
		for(int i=0 ; i<3 ; i++) {
			absval += translate2.components[i]*translate2.components[i];
		}
		translate2.components[1] += deltaTime*0.25f;
		if(Math.sqrt(absval) > 2) {
			translate2.components[0] = translate2.components[1] = translate2.components[2] = 0;
		}
		
		if(Err.hasError()) {
			Err.e("Not rendering, an error occured");
			return;
		}
		
		//init surface
		screentarget.setViewport();
		Target.clearTarget();
		
		//use program
		shader.use();
		
		//bind variables
		translate.use(shader);
		
		mesh.render(shader);
		mesh2.render(shader);
		
		//use another program
		shader2.use();
		
		//bind variables
		
		mesh.render(shader2);
		
		//use a third program with another mesh
		//and a texture
		shader3.use();
		
		//bind variables
		texture.use(shader3);
		texture2.use(shader3);
		
		mesh3.render(shader3);
		
		//do offline rendering
		
		target.beginCapture(shader, new LinkedList<Texture>(Arrays.asList(targetTexture)));
		target.setViewport();
		Target.clearTarget();
		
		shader.use();
		translate2.use(shader);
		
		mesh2.render(shader);
		
		target.endCapture();
		
		//render rendered texture to screen
		
		screentarget.setViewport();
		
		shader4.use();
		
		targetTexture.use(shader4);
		
		mesh3.render(shader4);
		
		//copy data into texture
		
		for(int i=0 ; i<cpubuffer.length/4 ; i++) {
			cpubuffer[i*4] = (byte)(255.0 * Math.random());
			cpubuffer[i*4+1] = (byte)(255.0 * Math.random());
			cpubuffer[i*4+2] = (byte)(255.0 * Math.random());
		}
		buffer.put(cpubuffer).position(0);
		cpucopytex.changeImage(buffer);
		
		//render another texture
		
		shader5.use();
		
		cpucopytex.use(shader5);
		
		mesh3.render(shader5);
		
	}

	@Override
	public void onSurfaceChanged(GL10 arg0, int width, int height) {
		screentarget.setWidth(width);
		screentarget.setHeight(height);
		screentarget.setRenderToDisplay(true);
		screentarget.compile();
	}

}
